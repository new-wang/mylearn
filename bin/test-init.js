#!/usr/bin/env node
const path = require('path');
const program = require('commander');
const chalk = require('chalk');
const exists = require('fs').existsSync;
const inquirer = require('inquirer');
const rm = require('rimraf').sync;
const ora = require('ora');
const download = require('download-git-repo'); //https://www.npmjs.com/package/download-git-repo

program
	.usage('<template-name> <project-name>')
	.parse(process.argv);
	
/* 
	help
	使用test init 如果 参数少于两个则显示该help 使用示例
 */	
program.on('--help',function(){
	console.log(' Examples');
	console.log();
	console.log(chalk.gray('   # create a new front-end frame'));
	console.log(chalk.green('  $ test init website my-project or $ chrys init react my-project'));
	console.log();
});

/* 
	hinit
 */
if (program.args.length < 2) return program.help();

// 获取参数及存放位置
/*template name*/
let template = program.args[0];

/*project name*/
let projectName = program.args[1];

/* dist 存放位置 */
let to = path.resolve(projectName);

console.log();
process.on('exit', function () {
    console.log('离开进程');
});

/*ask user override?
存放位置已存在文件夹，询问用户是否覆盖
*/
if(exists(to)){
	console.log(to)
	inquirer.prompt([{
		type:'confirm',
		message:'文件夹存在，是否覆盖？',
		name:'ok'
	}]).then(answers => {
		if(answers.ok){
			console.log('覆盖');
			run();
		}
	})
}else{
	console.log('文件夹不存在');
	run();
}

/* download template 下载模板 */
function run(){
	const spinner = ora('正在下载模板...');
	spinner.start();
	
	if (exists(to)) rm(to);
	// download(`direct:https://gitlab.com/smallproduct/sp_yongan.git`, to, {clone: true}, function (err) {
	//         spinner.stop()
	//         if (err) {
	// 			console.log('err',err);
	//             console.log(chalk.red('download error!'));
	//             process.exit(1);
	//         }
	//         console.log(chalk.green('    download success!'));
	//         console.log(chalk.green(`    plz cd  ${to}`));
	//         console.log(chalk.green('    $npm install '));
	//         console.log(chalk.green('    $npm start '));
	//     })
	/* 示例如下： */
	download(`direct:https://github.com/Chryseis/chryseis-cli`, to, {clone: true}, function (err) {
	        spinner.stop()
	        if (err) {
				console.log('err',err);
	            console.log(chalk.red('download error!'));
	            process.exit(1);
	        }
	        console.log(chalk.green('    download success!'));
	        console.log(chalk.green(`    plz cd  ${to}`));
	        console.log(chalk.green('    $npm install '));
	        console.log(chalk.green('    $npm start '));
	    })
}









