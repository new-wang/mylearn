#!/usr/bin/env node
const program = require('commander');
const version = require('../package.json').version;

program.version(version,'-v, --version')
		.command('init','init a react template')
		// .action((name) => {
		// 	console.log(name);
		// });
program.parse(process.argv);
/* 调用 version('1.0.0', '-v, --version') 会将 -v 和 --version 添加到命令中，可以通过这些选项打印出版本号。
调用 command('init <name>') 定义 init 命令，name 则是必传的参数，为项目名。
action() 则是执行 init 命令会发生的行为，要生成项目的过程就是在这里面执行的，这里暂时只打印出 name。
其实到这里，已经可以执行 init 命令了 */
